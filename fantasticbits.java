import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Prev 15.56
 * #Gold League
 * Grab Snaffles and try to throw them through the opponent's goal!
 * Move towards a Snaffle and use your team id to determine where you need to throw it.
 **/
class Player {

    public static class Entitie{

        private int id;
        private String entityType;
        private int x;
        private int y;
        private int vx;
        private int vy;
        private int state;
        
        public Entitie(int id, String entityType, int x, int y, int vx, int vy, int state){
            this.id=id;
            this.entityType=entityType;
            this.x=x;
            this.y=y;
            this.vx=vx;
            this.vy=vy;
            this.state=state;
        }
        
        public int getId(){
            return this.id;
        }

        public String getEntityType(){
            return this.entityType;
        }

        public int getX(){
            return this.x;
        }

        public int getY(){
            return this.y;
        }

        public int getVX(){
            return this.vx;
        }

        public int getVY(){
            return this.vy;
        }

        public int getState(){
            return this.state;
        }
        
        public void setState(int state){
            this.state = state;
        }
        
        public double getSquareDistance(Entitie e){
            
            double squareDistance;
            
            squareDistance = Math.pow(x-e.getX(),2)+Math.pow(y-e.getY(),2);            
            
            return squareDistance;
        }
    }

    private static int map[][]=new int[16][8];
    private static int magic =0;
    
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int myTeamId = in.nextInt(); // if 0 you need to score on the right of the map, if 1 you need to score on the left
        
        // game loop
        while (true) {
            for(int i=0;i<16;i++){
                for(int j=0;j<8;j++){
                    Player.map[i][j]=0;
                }
            }
            ArrayList<Entitie> myWizard = new ArrayList<>();
            ArrayList<Entitie> snaffle = new ArrayList<>();
            ArrayList<Entitie> bludgers = new ArrayList<>();
            ArrayList<Entitie> opponentWizard = new ArrayList<>();
            int myIds[]=new int[2];
            int f=0;
            int entities = in.nextInt(); // number of entities still in game
            for (int i = 0; i < entities; i++) {
                Entitie e = new Entitie(in.nextInt(),in.next(),in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt());
                switch(e.getEntityType()){
                    case "WIZARD":
                        myWizard.add(e);
                        myIds[f==0?0:1]=e.getId();
                        f++;
                        break;
                    case "OPPONENT_WIZARD":
                        opponentWizard.add(e);
                        break;
                    case "SNAFFLE":
                        snaffle.add(e);
                        break;
                    case "BLUDGER":
                    default:
                        bludgers.add(e);
                        break;
                }
                // entity identifier
                // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
                // position X
                // position Y
                // velocity X
                // velocity Y
                // 1 if the wizard is holding a Snaffle, 0 otherwise
            }
            
            for (int i = 0; i < 2; i++) {
                
                Entitie player = myWizard.get(i);
                // Write an action using System.out.println()
                // To debug: System.err.println("Debug messages...");
                Entitie closestSnaffle = getClosestSnaffle(snaffle,player);
                flipendoGoal(closestSnaffle,player,myTeamId);
                if(Player.magic>=20&&(myTeamId==0?closestSnaffle.getX()>player.getX():closestSnaffle.getX()<player.getX())&&closestSnaffle.getState()==1){
                    System.out.println("FLIPENDO "+closestSnaffle.getId()+setPlayer(player.getId()));
                    Player.magic-=20;
                }else if(Player.magic>=20&&(myTeamId==0?closestSnaffle.getX()<player.getX():closestSnaffle.getX()>player.getX())&&player.getSquareDistance(closestSnaffle)>(4000000)){
                    System.out.println("ACCIO "+closestSnaffle.getId()+setPlayer(player.getId()));
                    Player.magic-=20;
                }else{  
                    if((player.getState())!=0){
                        System.out.println("THROW "+(myTeamId==0?16000:0)+" 3750 500"+setPlayer(player.getId()));
                    }else{
                        System.out.println("MOVE "+closestSnaffle.getX()+" "+closestSnaffle.getY()+" 150"+setPlayer(player.getId()));
                    }
                    // Edit this line to indicate the action for each wizard (0 <= thrust <= 150, 0 <= power <= 500)
                    // i.e.: "MOVE x y thrust" or "THROW x y power"
                }
            }
            Player.magic++;
        }  
    }
    
    public static void flipendoGoal(Entitie closestSnaffle, Entitie player, int myTeamId){
        
        try{
            int coef =(closestSnaffle.getY()-player.getY())/(closestSnaffle.getX()-player.getX());
            int p = player.getY()-(coef*player.getX());
            int x=myTeamId==0?16000:0;

            int y = coef*x+p;
            
            
            if(y<6000&&y>2000&&(x==0?player.getX()>closestSnaffle.getX():player.getX()<closestSnaffle.getX())){
                closestSnaffle.setState(1);
            }
        }catch(Exception e){
            
        }
    }
    public static Entitie getClosestSnaffle(ArrayList<Entitie> snaffle,Entitie player){
        double minSquareDist=320000010;
        Entitie returnSnaffle=snaffle.get(0);
        for(Entitie s : snaffle){
            double squareDist = player.getSquareDistance(s);
            if(squareDist<minSquareDist){
                returnSnaffle=s;
                minSquareDist=squareDist;
            }
        }
        return returnSnaffle;
    }
    
    public static String setPlayer(int id){
        switch (id){
            case 0:
                return " A.Johnson";
            case 1:
                return " K.Bell";
            case 2:
                return " A.Pucey";
            case 3:
                return " M.Flint";
            default:
                return " Harry";
        }
    }
}
