import java.util.*;
import java.io.*;
import java.math.*;
 
class Player {

    public static int mapFactory[][];
    public static int factoryCount;


    public static ArrayList<Factory> myFactory;
    public static ArrayList<Factory> enemyFactory;
    public static ArrayList<Factory> neutralFactory;
    public static ArrayList<Factory> allFactory;

    public static ArrayList<Troop> myTroop;
    public static ArrayList<Troop> enemyTroop;

    public static ArrayList<Bomb> myBomb;
    public static ArrayList<Bomb> enemyBomb;


    public static void main(String args[]) {
        
        
        myTroop = new ArrayList<Troop>();
        enemyTroop = new ArrayList<Troop>();
        
        myFactory = new ArrayList<Factory>();
        enemyFactory = new ArrayList<Factory>();
        neutralFactory = new ArrayList<Factory>();
        allFactory = new ArrayList<Factory>();
        
        myBomb = new ArrayList<Bomb>();
        enemyBomb = new ArrayList<Bomb>();
        
        Scanner in = new Scanner(System.in);
        
        factoryCount = in.nextInt();
        int linkCount = in.nextInt();
        
        mapFactory = new int[factoryCount][factoryCount];
        
        for (int i = 0; i < linkCount; i++) {
            int factory1 = in.nextInt();
            int factory2 = in.nextInt();
            int distance = in.nextInt();
            
            mapFactory[factory1][factory2]=distance;
            mapFactory[factory2][factory1]=distance;
            
        }

        int loop=0;


        while (true) {
            
            
            int entityCount = in.nextInt();
            
            
            for (int i = 0; i < entityCount; i++) {
                
                int id = in.nextInt();
                String type = in.next();
                
                if(type.equals("FACTORY")){
                    
                    Factory f = new Factory(id,type,in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt());
                    allFactory.add(f);
                    
                    if(f.getOwner()==1){
                        myFactory.add(f);
                    }else if(f.getOwner()==0){
                        neutralFactory.add(f);
                    }else{
                        enemyFactory.add(f);
                    }
                    
                }else if(type.equals("TROOP")){
                    
                    Troop t = new Troop(id,type,in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt());
                    
                    if(t.getOwner()==1){
                        myTroop.add(t);
                    }else{
                        enemyTroop.add(t);
                    }
                    
                }else{
                    
                    Bomb b = new Bomb(id,type,in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt(),in.nextInt());
                    
                    if(b.getOwner()==1){
                        myBomb.add(b);
                    }else{
                        enemyBomb.add(b);
                    }
                }
            }


            allFactory.remove(myFactory);
            
            for(Factory f : allFactory){
                f.createDistanceScore();
            }
            
            
            String answer="";
            int i=0,j=0;


            if((neutralFactory.size()>0 || enemyFactory.size()>0)){
                
                answer+="WAIT";
                for(Factory f : myFactory){
                    
                    Integer[] integ = f.getScore();
                    int m =0;
                    
                    answer+=" ; ";
                    int k=0;
                    
                    if(f.getProduction()>=1||f.getPopulation()>10){
                        while(f.getPopulation()>allFactory.get(f.getMinimalScoreId(k)).getPopulation()){
                            int id = f.getEntityId();
                            int idDestination = f.getMinimalScoreId(k) ;
                            Factory g = allFactory.get(idDestination);
                            answer +=("MOVE "+id+" "+idDestination+" "+(g.getPopulation()+mapFactory[id][idDestination]*g.getProduction()));
                            answer+=" ; ";
                            k++;
                            
                        }
                        answer+="WAIT";
                    }else{
                        answer+="INC "+f.getEntityId();
                    }
                }
                
                
            
                if(loop==0||(loop==5)){
              
                
                    Collections.sort(enemyFactory, new Comparator<Factory>() {
                        @Override
                        public int compare(Factory tc1, Factory tc2) {
                            return tc1.getPopulation().compareTo(tc2.getPopulation());
                        }
                    });
                    Collections.reverse(enemyFactory);
                    
                    answer+= " ; BOMB "+enemyFactory.get(0).getClosest()+" "+enemyFactory.get(0).getEntityId();
                
                }
                
                System.out.println(answer);
            }else{
                
                System.out.println("WAIT");
            }
            
            
            myFactory.clear();
            neutralFactory.clear();
            enemyFactory.clear();
            allFactory.clear();
            
            myTroop.clear();
            enemyTroop.clear();
            
            myBomb.clear();
            enemyBomb.clear();
            
            loop++;
        }
    }
    
    public static class Entity{
        private int entityId;
        private String entityType;
        private int[] arguments=new int[5];
        
        public Entity(int eI,String eT,int a1,int a2,int a3,int a4,int a5){
            this.entityId=eI;
            this.entityType=eT;
            this.arguments[0]=a1;
            this.arguments[1]=a2;
            this.arguments[2]=a3;
            this.arguments[3]=a4;
            this.arguments[4]=a5;
        }
        
        public int getEntityId(){
            return this.entityId;
        }
        
        public String getEntityType(){
            return this.entityType;
        }
        
        public int[] getArguments(){
            return this.arguments;
        }
        
        public int getOwner(){
            return this.arguments[0];
        }
    }
    
    public static class MovingEntity extends Entity{
        
        public MovingEntity(int eI,String eT,int a1,int a2,int a3,int a4,int a5){
            super(eI,eT,a1,a2,a3,a4,a5);
        }
        
        public int getFrom(){
            return super.arguments[1];
        }
        
        public int getTo(){
            return super.arguments[2];
        }
        
        public int getFourthArgument(){
            return super.arguments[3];
        }
        
        public int getFifthArgument(){
            return super.arguments[4];
        }
    }
    
    public static class Factory extends Entity{
        
        private int distance[];
        private Integer score[];
        
        public Factory(int eI,String eT,int a1,int a2,int a3,int a4,int a5){
            super(eI,eT,a1,a2,a3,a4,a5);
        }
        
        public void createDistanceScore(){
            distance = new int[factoryCount];
            score = new Integer[factoryCount];
            
            for(int i=0;i<factoryCount;i++){
                distance[i]=mapFactory[super.getEntityId()][i];
                
                if(distance[i]==0){
                    score[i]=50000;
                }else{
                    if(allFactory.get(i).getOwner()==1){
                        if(allFactory.get(i).getProduction()==0){
                            score[i] = distance[i]*2 + allFactory.get(i).getPopulation()*4 - allFactory.get(i).getProduction()*3 + 50;
                        }else{
                            score[i] = distance[i]*2 + allFactory.get(i).getPopulation()*4 - allFactory.get(i).getProduction()*3;
                        }
                    }else{
                        if(allFactory.get(i).getProduction()==0){
                            score[i] = distance[i]*2 + allFactory.get(i).getPopulation()/3 - allFactory.get(i).getProduction()*3 + 50;
                        }else{
                            score[i] = distance[i]*2 + allFactory.get(i).getPopulation()/3 - allFactory.get(i).getProduction()*3;
                        }
                    }
                }
            }
        }
        
        public Integer getPopulation(){
            return Integer.parseInt(super.arguments[1]+"");
        }
        
        public Integer getProduction(){
            return Integer.parseInt(super.arguments[2]+"");
        }
        
        public int[] getDistance(){
            return this.distance;
        }
        
        public int getClosest(){
            int[] alldistance = this.getDistance();
            int dmin=50;
            int idmin=0;
            for(int i=0;i<alldistance.length;i++){
                if(dmin>alldistance[i]&&alldistance[i]!=0&&allFactory.get(i).getOwner()==1){
                    dmin = alldistance[i];
                    idmin=i;
                }
            }
            
            return idmin;
        
        }
        
        public Integer[] getScore(){
            return this.score;
        }
        
        public int getMinimalScoreId(int i){
            
            ArrayIndexComparator comparator = new ArrayIndexComparator(this.score);
            Integer[] indexes = comparator.createIndexArray();
            Arrays.sort(indexes, comparator);
            
            return indexes[i];
        }
        
    }
    
    public static class Troop extends MovingEntity{
        
        public Troop(int eI,String eT,int a1,int a2,int a3,int a4,int a5){
            super(eI,eT,a1,a2,a3,a4,a5);
        }
        
        public int getCount(){
            return super.getFourthArgument();
        }
        
        public int getTimeLeft(){
            return super.getFifthArgument();
        }
        
    }
    
    public static class Bomb extends MovingEntity{
        
        public Bomb(int eI,String eT,int a1,int a2,int a3,int a4,int a5){
            super(eI,eT,a1,a2,a3,a4,a5);
        }
        
        public int getTimeLeft(){
            return super.getFourthArgument();
        }
        
    }
    
    public static class ArrayIndexComparator implements Comparator<Integer>
    {
        private final Integer[] array;
    
        public ArrayIndexComparator(Integer[] array)
        {
            this.array = array;
        }
    
        public Integer[] createIndexArray()
        {
            Integer[] indexes = new Integer[array.length];
            for (int i = 0; i < array.length; i++)
            {
                indexes[i] = i;
            }
            return indexes;
        }
    
        @Override
        public int compare(Integer index1, Integer index2)
        {
            return array[index1].compareTo(array[index2]);
        }
    }
}
