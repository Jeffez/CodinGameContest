import java.util.*;
import java.io.*;
import java.math.*;

class Player {
    /*Constant*/
    private static int KILLZONE = 4000000;//Initialize the constant KILLZONE to 4 000 000 (The square distance of the danger zone(2000 units))

    /* Main method */
    public static void main(String args[]) {

        Scanner in = new Scanner(System.in);
        ArrayList<int[]> dataPoint = new ArrayList<int[]>(); // Contains all my Data point
        ArrayList<int[]> enemy = new ArrayList<int[]>();// Contains all my Enemy
        int turn =0;
        double rand= ((Math.random()*2));
        //Game loop
        while (true) {
            turn++; // Used to know which turn we are
            int x = in.nextInt();
            int y = in.nextInt();
            int dataCount = in.nextInt();
            for (int i = 0; i < dataCount; i++) {
                int info[] = new int[4];
                info[0] = in.nextInt(); // Id
                info[1] = in.nextInt(); // X
                info[2] = in.nextInt(); // Y
                info[3] = 0;
                /*In the first turn, I stock all the information of the Data point in my dataPoint's ArrayList*/
                if(turn==1){
                    dataPoint.add(info);
                }
            }
            int enemyCount = in.nextInt();
            for (int i = 0; i < enemyCount; i++) {
                int info[] = new int[9];
                info[0] = in.nextInt(); // Id
                info[1] = in.nextInt(); // X
                info[2] = in.nextInt(); // Y
                info[3] = in.nextInt(); // Life
                info[4] = squareDistance(x,y,info[1],info[2]);// Square distance between Wolf and the enemy i
                info[5] = HowManyTurnINeedToKillHim(info[3],info[4]); // NOT USE . Computes how many turn Wolf need to kill the enemy if he doesn't move, and the enemy doesn't move
                for(int j=0;j<dataPoint.size();j++){//Test for each data point if he is lost
                    if(info[1]==dataPoint.get(j)[1]&&info[2]==dataPoint.get(j)[2]){//If yes, remove him from the ArrayList which contains all the data point
                        dataPoint.get(j)[3]=1;
                    }
                }
                /*Add the information of each enemy in the enemy's ArrayList*/
                if(info[3]>0){
                    enemy.add(info);
                }
            }

            for(int i=0;i<enemyCount;i++){

                enemy.get(i)[6] = direction(enemy.get(i)[1],enemy.get(i)[2],dataPoint); //The Data point focus by the enemy
                enemy.get(i)[7] =(int) (enemy.get(i)[1]+(dataPoint.get(enemy.get(i)[6])[1]-enemy.get(i)[1])*(500/(float)distance(squareDistance(enemy.get(i)[1],enemy.get(i)[2],dataPoint.get(enemy.get(i)[6])[1],dataPoint.get(enemy.get(i)[6])[2]))));//Next X
                enemy.get(i)[8] =(int) (enemy.get(i)[2]+(dataPoint.get(enemy.get(i)[6])[2]-enemy.get(i)[2])*(500/(float)distance(squareDistance(enemy.get(i)[1],enemy.get(i)[2],dataPoint.get(enemy.get(i)[6])[1],dataPoint.get(enemy.get(i)[6])[2]))));//Next Y

            }

            Collections.sort(enemy, (int[] o1, int[] o2) -> o1[4] - o2[4]); //Sort the enemy's ArrayList by the distance between them and Wolf

            boolean oneshot=false;//Create the variable oneshot and initialize it to false
            int idshoot=0;//Initialize the variable idshoot to 0

            for(int i=0;i<enemy.size();i++){//Loop to test all the enemy
                if(CanIKillHimInOneShot(enemy.get(i))){ //Test if Wolf can kill the guard "i" in only one shoot
                    idshoot=enemy.get(i)[0]; //Attribute the value of the enemy he can one shot to the variable idshoot
                    oneshot=true;//Change the value of oneshot to true
                    break;//Stop the loop
                }
            }
            if(AmIInDanger(enemy,x,y)){//If there is an enemy closer thant 2500units, escape
                    escape(enemy,x,y,0,0,0,x,y);//Try to escape the closest enemy by moving in the oposite direction
            }else{//Else, test if he can one shot an enemy
                if(oneshot){//If yes, shot him
                    System.out.println("SHOOT "+idshoot);
                }else{//Else, test if is a distance superior to 4890
                    int nextX =(int) (x+(enemy.get(0)[7]-x)*(1000/(float)distance(squareDistance(x,y,enemy.get(0)[7],enemy.get(0)[8]))));
                    int nextY =(int) (y+(enemy.get(0)[8]-y)*(1000/(float)distance(squareDistance(x,y,enemy.get(0)[7],enemy.get(0)[8]))));
                    if(rand<=1){
                        if(enemy.get(0)[4]>23912100){//If yes, move in direction of this enemy
                            System.out.println("MOVE "+nextX+" "+nextY);
                        }else{//Else, shot the closest enemy
                            System.out.println("SHOOT "+enemy.get(0)[0]);
                        }
                    }else{
                      if(!AmIInDanger(enemy,nextX,nextY,dataPoint)){//If yes, move in direction of this enemy
                          System.out.println("MOVE "+nextX+" "+nextY);
                      }else{//Else, shot the closest enemy
                          System.out.println("SHOOT "+enemy.get(0)[0]);
                      }
                    }
                }
            }
            enemy.clear(); //Clear enemy, because all the enemy will be updated
        }
    }

    /*Try to escape the closest enemy by moving in the oposite direction*/
    public static void escape(ArrayList<int[]> enemy,int x,int y,int sumx,int sumy,int dangerousenemy,int px,int py){
        //Initialise all the variable we need to 0
        int ex=0;//The x to avoid
        int ey=0;//The y to avoid

        for(int i=0;i<enemy.size();i++){//For each enemy, add the enemy's x and the enemy's y to the respective sum
            int dist = squareDistance(x,y,enemy.get(i)[7],enemy.get(i)[8]);//Computes the square distance between Wolf and the next position
            if(dist<KILLZONE){//If this distance is inferior to the KILLZONE, Wolf is in danger, return true
              sumx+=enemy.get(i)[7];
              sumy+=enemy.get(i)[8];
              dangerousenemy++;
            }
        }

        //Computes the coordonnates to avoid
        ex=sumx/dangerousenemy;
        ey=sumy/dangerousenemy;


        x=px-(ex-px);
        y=py-(ey-py);

        if(AmIInDanger(enemy,x,y)){
            escape(enemy,x,y,sumx,sumy,dangerousenemy,px,py);
        }else{
          System.out.println("MOVE "+x+" "+y);//Move to the new coordonates
        }



    }

    /*Test if Wolf can kill the enemy in only one shoot*/
    public static boolean CanIKillHimInOneShot(int enemy[]){

        int dist = distance(enemy[4]);//Initialize the value of dist by the value bewtween Wolf and the enemy
        int life = enemy[3];//Initialize the value life by the value of the life of the enemy

        float damage = (float) (125000/Math.pow(dist,1.2));//Computes the dammages made to the enemy

        if(life-damage<0){//If the life is under the damage, we can one shot him, so return true
            return true;
        }else{//Else return false
            return false;
        }

    }

    /*Computes how many turn Wolf need to kill the enemy if he doesn't move, and the enemy doesn't move*/
    public static int HowManyTurnINeedToKillHim(int life, int distance){
        for(int i=1;i<10;i++){//Make a loop of 10 turns
            life =(int) (life-(125000/Math.pow(distance,1.2)));//Computes the new life at turn i, for the distance between Wolf and the enemy
            if(life<0){//If my new life is under 0, then, the number of turn to kill the enemy is i
                return i;
            }
        }//If the loop end, then, the turn needed to kill him is superior to 10. Then return 10
        return 10;
    }

    /*Return the square distance, when you ask him with the value of two point*/
    public static int squareDistance(int x,int y,int x2,int y2){
        return (int) (Math.pow(x-x2,2)+Math.pow(y-y2,2));
    }

    /*Return the value of the distance when you ask him with the square distance*/
    public static int distance(int x){
        return (int) Math.sqrt(x);
    }

    /*Simulate the behavior of the enemy, and return the id of the Datapoint targeted*/
    public static int direction(int x, int y, ArrayList<int[]> dataPoint){
        int minDist=625000000;//Initialize the variable minDist to 25000^2 units (It's th minimal distance square)
        int idFocus=0;//Initialize the focus to 0
        for(int i=0;i<dataPoint.size();i++){//For each datapoint
            int dist= squareDistance(x,y,dataPoint.get(i)[1],dataPoint.get(i)[2]);//Computes the squareDistance between the enemy and the datapoint "i"
            if(dist<minDist&&dataPoint.get(i)[3]==0){//If this square distance is inferior to the minDist
                minDist=dist;//minDist become the squaredistance
                idFocus=dataPoint.get(i)[0];//The enemy's focus became the dataPoint's id
            }
        }
        return idFocus;
    }

    /*Test if Wolf is in danger if he doesn't move*/
    public static boolean AmIInDanger(ArrayList<int[]> enemy,int x, int y){
        for(int i=0;i<enemy.size();i++){//For each enemy
            int dist = squareDistance(x,y,enemy.get(i)[7],enemy.get(i)[8]);//Computes the square distance between Wolf and the next position
            if(dist<KILLZONE||dist<KILLZONE){//If this distance is inferior to the KILLZONE, Wolf is in danger, return true
                return true;
            }
        }
        return false;//Wolf isn't in danger if any enemy's next position is superior to the KILLZONE. Return false.
    }

    public static boolean AmIInDanger(ArrayList<int[]> enemy,int x, int y,ArrayList<int[]> dataPoint){
        for(int i=0;i<enemy.size();i++){
            for(int j=0;j<dataPoint.size();j++){//Test for each data point if he is lost
                if(enemy.get(i)[7]==dataPoint.get(j)[1]&&enemy.get(i)[8]==dataPoint.get(j)[2]){//If yes, remove him from the ArrayList which contains all the data point
                    dataPoint.get(j)[3]=1;
                }
            }
        }
        for(int i=0;i<enemy.size();i++){//For each enemy
            int dist = squareDistance(x,y,enemy.get(i)[7],enemy.get(i)[8]);//Computes the square distance between Wolf and the next position

            enemy.get(i)[6] = direction(enemy.get(i)[7],enemy.get(i)[8],dataPoint); //The Data point focus by the enemy
            int nextX=(int) (enemy.get(i)[7]+(dataPoint.get(enemy.get(i)[6])[1]-enemy.get(i)[7])*(500/(float)distance(squareDistance(enemy.get(i)[7],enemy.get(i)[8],dataPoint.get(enemy.get(i)[6])[1],dataPoint.get(enemy.get(i)[6])[2]))));//Next X
            int nextY=(int) (enemy.get(i)[8]+(dataPoint.get(enemy.get(i)[6])[2]-enemy.get(i)[8])*(500/(float)distance(squareDistance(enemy.get(i)[7],enemy.get(i)[8],dataPoint.get(enemy.get(i)[6])[1],dataPoint.get(enemy.get(i)[6])[2]))));//Next Y
            int dist2 = squareDistance(x,y,enemy.get(i)[7],enemy.get(i)[8]);
            if(dist<KILLZONE||dist2<KILLZONE){//If this distance is inferior to the KILLZONE, Wolf is in danger, return true
                return true;
            }
        }
        return false;//Wolf isn't in danger if any enemy's next position is superior to the KILLZONE. Return false.
    }
}
