import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int width = in.nextInt();
        int height = in.nextInt();
        int myId = in.nextInt();
        in.nextLine();
        char map[][] = new char[13][11];
        ArrayList<int[]> visitableArea = new ArrayList<int[]>();
        int prevX=0;
        int prevY=0;
        // game loop
        while (true) {
            for (int i = 0; i < height; i++) {
                String row = in.nextLine();
                for(int j=0;j<row.length();j++){
                    char chara = row.charAt(j);
                    map[j][i]=chara;
                }
            }
            int entities = in.nextInt();
            int myX=0;
            int myY=0;
            int scope=0;
            ArrayList<int[]> bomb = new ArrayList<int[]>();
            for (int j = 0; j < entities; j++) {
                int entityType = in.nextInt();
                int owner = in.nextInt();
                int x = in.nextInt();
                int y = in.nextInt();
                int param1 = in.nextInt();
                int param2 = in.nextInt();
                if(entityType==0&&owner==myId){
                  myX=x;
                  myY=y;
                  scope=param2;
                }
                if(entityType==1){
                    map[x][y]='B';
                    whereThereIsABomb(bomb,map,x,y,param1,param2);
                }
            }
            whereCanIGo(visitableArea,map,myX,myY);
            in.nextLine();
            String nextDestination=nearestSpot(visitableArea,map,scope,myX,myY,myId);

            String r="";
            String out="";
            String nD[] = nextDestination.split(" ");
            if(Integer.parseInt(nD[0])==myX&&Integer.parseInt(nD[1])==myY){
                r="BOMB ";
            }else{
                r="MOVE ";
            }
            r+=nD[0]+" "+nD[1]+out;
            int info[]=new int[2];
            info[0]=Integer.parseInt(nD[0]);
            info[1]=Integer.parseInt(nD[1]);

            if(contains(bomb,info)){
                nextDestination=escape(visitableArea,bomb,myX,myY);
                String nD2[] = nextDestination.split(" ");
                r="MOVE "+nD2[0]+" "+nD2[1]+" ESCAPE";
            }

            if(!nextSpot(bomb,myX,myY,prevX,prevY)){
                nextDestination=escape(visitableArea,bomb,myX,myY);
                String nD2[] = nextDestination.split(" ");
                r="MOVE "+nD2[0]+" "+nD2[1]+" ESCAPE";
            }

            System.out.println(r);
            visitableArea.clear();
            prevX=myX;
            prevY=myY;
        }
    }

    public static void whereThereIsABomb(ArrayList<int[]> bomb,char map[][],int myX, int myY,int left,int scope){
      int coord2[] = new int[3];
      coord2[0]=myX;
      coord2[1]=myY;
      coord2[2]=left;
      if(!contains(bomb,coord2)){
          bomb.add(coord2);
      }
      for(int i=0;i<scope;i++){
          if(myX-i>=0){
              int coord[] = new int[3];
              coord[0]=myX-i;
              coord[1]=myY;
              coord[2]=left;
              if(map[coord[0]][coord[1]]=='.'){
                  bomb.add(coord);
              }
          }
          if(myX+i<=12){
              int coord[] = new int[3];
              coord[0]=myX+i;
              coord[1]=myY;
              coord[2]=left;
              if(map[coord[0]][coord[1]]=='.'){
                  bomb.add(coord);
              }
          }
          if(myY-i>=0){
              int coord[] = new int[3];
              coord[0]=myX;
              coord[1]=myY-i;
              coord[2]=left;
              if(map[coord[0]][coord[1]]=='.'){
                  bomb.add(coord);
              }
          }
          if(myY+i<=10){
              int coord[] = new int[3];
              coord[0]=myX;
              coord[1]=myY+i;
              coord[2]=left;
              if(map[coord[0]][coord[1]]=='.'){
                  bomb.add(coord);
              }
          }
      }
    }

    //Test all the possible way to know where the bot can go
    public static void whereCanIGo(ArrayList<int[]> visitableArea,char map[][],int myX, int myY){
        int coord2[] = new int[2];
        coord2[0]=myX;
        coord2[1]=myY;
        if(!contains(visitableArea,coord2)){
            visitableArea.add(coord2);
        }
        if(myX>0){
            int coord[] = new int[2];
            coord[0]=myX-1;
            coord[1]=myY;
            if(map[coord[0]][coord[1]]=='.'&&!contains(visitableArea,coord)){
                whereCanIGo(visitableArea,map,coord[0],coord[1]);
            }
        }
        if(myX<12){
            int coord[] = new int[2];
            coord[0]=myX+1;
            coord[1]=myY;
            if(map[coord[0]][coord[1]]=='.'&&!contains(visitableArea,coord)){
                whereCanIGo(visitableArea,map,coord[0],coord[1]);
            }
        }
        if(myY>0){
            int coord[] = new int[2];
            coord[0]=myX;
            coord[1]=myY-1;
            if(map[coord[0]][coord[1]]=='.'&&!contains(visitableArea,coord)){
                whereCanIGo(visitableArea,map,coord[0],coord[1]);
            }
        }
        if(myY<10){
            int coord[] = new int[2];
            coord[0]=myX;
            coord[1]=myY+1;
            if(map[coord[0]][coord[1]]=='.'&&!contains(visitableArea,coord)){
                whereCanIGo(visitableArea,map,coord[0],coord[1]);
            }
        }
    }

    //check to found the nearest spot with the highest point
    public static String nearestSpot(ArrayList<int[]> visitableArea,char map[][],int scope,int myX, int myY,int myId){

        ArrayList<int[]> nearestArea = new ArrayList<int[]>();

        for(int i=0;i<visitableArea.size();i++){
            int vXA = visitableArea.get(i)[0];
            int vYA = visitableArea.get(i)[1];
            int dist=Math.abs(vXA-myX)+Math.abs(vYA-myY);//Distance from my position to the position tested
            if(dist<8){
                int coord[] = new int[2];
                coord[0]=vXA;
                coord[1]=vYA;
                nearestArea.add(coord);
            }
        }

        List<int[]> distance =new ArrayList<int[]>();

        for(int i=0;i<nearestArea.size();i++){
            int x = nearestArea.get(i)[0];
            int y = nearestArea.get(i)[1];
            boolean LEFT = true;
            boolean RIGHT = true;
            boolean TOP = true;
            boolean DOWN = true;
            int point=0;
            for(int j=0;j<scope;j++){
              if(x+j<=12){
                if(RIGHT&&(map[x+j][y]=='0'||map[x+j][y]=='1'||map[x+j][y]=='2')){
                    RIGHT=false;
                    point++;
                }else if(map[x+j][y]=='X'){
                    RIGHT=false;
                }
              }
              if(x-j>=0){
                if(LEFT&&(map[x-j][y]=='0'||map[x-j][y]=='1'||map[x-j][y]=='2')){
                    LEFT=false;
                    point++;
                }else if(map[x-j][y]=='X'){
                    LEFT=false;
                }
              }
              if(y-j>=0){
                if(TOP&&(map[x][y-j]=='0'||map[x][y-j]=='1'||map[x][y-j]=='2')){
                    TOP=false;
                    point++;
                }else if(map[j][y-j]=='X'){
                    TOP=false;
                }
              }
              if(y+j<=10){
                if(DOWN&&(map[x][y+j]=='0'||map[x][y+j]=='1'||map[x][y+j]=='2')){
                    DOWN=false;
                    point++;
                }else if(map[x][y+j]=='X'){
                    DOWN=false;
                }
              }
            }
            int dist[]= new int[4];
            dist[0]=Math.abs(x-myX)+Math.abs(y-myY);//Distance from my position to the position tested
            dist[1]=point;//Score of the position tested
            dist[2]=x;
            dist[3]=y;
            distance.add(dist);
        }
        Collections.sort(distance, new Comparator<int[]>() { @Override public int compare(int[] o1, int[] o2) { return o1[1] - o2[1]; } });
        Collections.sort(distance, (int[] o1, int[] o2) -> o2[1] - o1[1]);

        if(visitableArea.size()<=3){
            distance.remove(0);
            if(myId==1){
                return "11 10";
            }
        }
            return distance.get(0)[2]+" "+distance.get(0)[3];


    }

    //Test if al contains the array
    public static boolean contains(ArrayList<int[]> al, int[] array){
        for(int i=0;i<al.size();i++){
            if(al.get(i)[0]==array[0]&&al.get(i)[1]==array[1]){
                return true;
            }
        }
        return false;
    }

    //Test if we need to escape
    public static String escape(ArrayList<int[]> visitableArea,ArrayList<int[]> bomb,int myX,int myY){
        ArrayList<int[]> distance = new ArrayList<int[]>();
        for(int i=0;i<visitableArea.size();i++){
            int vXA = visitableArea.get(i)[0];
            int vYA = visitableArea.get(i)[1];
            int dist=Math.abs(vXA-myX)+Math.abs(vYA-myY);//Distance from my position to the position tested
            int coord[] = new int[3];
            coord[0]=vXA;
            coord[1]=vYA;
            coord[2]=dist;
            if(!contains(bomb,coord)){
                distance.add(coord);
            }
        }
        Collections.sort(distance, (int[] o1, int[] o2) -> o1[2] - o2[2]);
        try{
          return distance.get(0)[0]+" "+distance.get(0)[1];
        }catch(Exception e){
          return myX+" "+myY;
        }
    }

    public static boolean containScope(ArrayList<int[]> al, int[] array){
        for(int i=0;i<al.size();i++){
            if(al.get(i)[0]==array[0]&&al.get(i)[1]==array[1]&&al.get(i)[2]<=2){
                return true;
            }
        }
        return false;
    }

    public static boolean nextSpot(ArrayList<int[]> bomb, int myX, int myY, int prevX, int prevY){
        if(myX>0){
            int coord[] = new int[2];
            coord[0]=myX-1;
            coord[1]=myY;
            if(containScope(bomb,coord)){
              return false;
            }
        }
        if(myX<12){
            int coord[] = new int[2];
            coord[0]=myX+1;
            coord[1]=myY;
            if(containScope(bomb,coord)){
              return false;
            }
        }
        if(myY>0){
            int coord[] = new int[2];
            coord[0]=myX;
            coord[1]=myY-1;
            if(containScope(bomb,coord)){
              return false;
            }
        }
        if(myY<10){
            int coord[] = new int[2];
            coord[0]=myX;
            coord[1]=myY+1;
            if(containScope(bomb,coord)){
              return false;
            }
        }
        return true;
    }
}
