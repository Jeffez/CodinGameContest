import java.util.*;
import java.io.*;
import java.math.*;

class Player {
    
    public static ArrayList<Unit> tankers = new ArrayList<Unit>();
    
    public static ArrayList<Master> players = new ArrayList<Master>();
    public static ArrayList<Master> previousPlayers = new ArrayList<Master>();
    
    public static ArrayList<Skill> wrecks = new ArrayList<Skill>();
    public static ArrayList<Skill> tarPools = new ArrayList<Skill>();
    public static ArrayList<Skill> oilPools = new ArrayList<Skill>();
    
    public static Skill bestReaper;
    public static Skill bestDestroyer;
    public static Skill bestDoof;
    
    public static long time = 0;
    
    public static void main(String args[]) {
        
        Scanner in = new Scanner(System.in);

        
        // game loop
        while (true) {
            time = System.currentTimeMillis();
            int myScore = in.nextInt();
            int enemyScore1 = in.nextInt();
            int enemyScore2 = in.nextInt();
            int myRage = in.nextInt();
            int enemyRage1 = in.nextInt();
            int enemyRage2 = in.nextInt();
            
            players.add(new Master(myScore,myRage));
            players.add(new Master(enemyScore1,enemyRage1));
            players.add(new Master(enemyScore2,enemyRage2));
            
            int unitCount = in.nextInt();
            
            bestReaper = new Skill(0, 0, 0, 0);
            bestDestroyer = new Skill(0, 0, 0, 0);
            bestDoof = new Skill(0, 0, 0, 0);
            for (int i = 0; i < unitCount; i++) {
                
                int unitId = in.nextInt();
                int unitType = in.nextInt();
                int player = in.nextInt();
                float mass = in.nextFloat();
                int radius = in.nextInt();
                int x = in.nextInt();
                int y = in.nextInt();
                int vx = in.nextInt();
                int vy = in.nextInt();
                int extra = in.nextInt();
                int extra2 = in.nextInt();
            
            
                switch(unitType){
                    
                    case 0:
                        players.get(player).reaper = new Unit(x, y, mass, radius, vx, vy, extra, extra2);
                        break;
                    case 1:
                        players.get(player).destroyer = new Unit(x, y, mass, radius, vx, vy, extra, extra2);                
                        break;
                    case 2:
                        players.get(player).doof = new Unit(x, y, mass, radius, vx, vy, extra, extra2);
                        break;
                    case 3:
                        tankers.add(new Unit(x, y, mass, radius, vx, vy, extra, extra2));
                        break;
                    case 4:
                        wrecks.add(new Skill(x, y, radius, extra));
                        break;
                    case 5:
                        tarPools.add(new Skill(x, y, radius, extra));
                        break;
                    case 6:
                        oilPools.add(new Skill(x, y, radius, extra));
                        break;
                    default:
                        System.err.println("Error : UnitType [Add]");
                        
                }
                
            }
            
            scoring();
            brake();
            System.err.println(System.currentTimeMillis()-time);
            System.out.println(bestReaper.x + " " + bestReaper.y + " 300");
            System.out.println(bestDestroyer.x + " " + bestDestroyer.y + " 300");
            if(myRage > 30 && players.get(0).doof.distance(bestDoof) < 2000 && willEarnWater(bestDoof)){
                System.out.println("SKILL " + (bestDoof.x - players.get(0).doof.vx) + " " + (bestDoof.y - players.get(0).doof.vy));
            }else{
                System.out.println(bestDoof.x + " " + bestDoof.y + " 300");
            }
            clean();
        
        }
        
    }
    
    public static void scoring() {
        
        Master me = players.get(0);
        
        //Delete Wreck where there is Oil
        for( int l = 0 ; l < wrecks.size() ; l++) {
            Skill w = wrecks.get(l);
            for( Skill o : oilPools ) {
                if(w.distance(o) < w.radius/2){
                    w.score += o.extra;
                }
            }
        }
        
        //Choosing best objective for Destroyer
        double scoreTanker = 6000;
        Point center = new Point(0,0);
        for( Unit t : tankers ){
            
            double myDistance = t.distance(me.destroyer);
            double reaperDistance = t.distance(me.reaper);
            double enemyDistance = t.distance(players.get(1).reaper) + t.distance(players.get(2).reaper);
            double enemyDistanceDoof = t.distance(players.get(1).doof) + t.distance(players.get(2).doof);
            double tankerDistance = t.distance(center);
            int water = t.extra;
            double score =  reaperDistance * 5 - enemyDistance - water*1250 + tankerDistance;
            
            if(score < scoreTanker){
                
                scoreTanker = score;
                bestDestroyer = new Skill(t.x, t.y, t.radius, t.extra);
                
            }
            
        }
        
        if(tankers.size() == 0){
            if(bestPlayerId()!=0){
                bestDestroyer = new Skill(players.get(bestPlayerId()).reaper);
            }else{
                bestDestroyer = new Skill(players.get(1).reaper);
            }
            
        }
        
        // Scoring the map. The reaper choose the pos with the best Score
        double scoreWreck = 6000;
        
        for( Skill w : wrecks ){
            if(System.currentTimeMillis()-time>40){
                break;
            }
            double myDistance = w.distance(me.reaper);
            double enemyDistance = w.distance(players.get(1).reaper) + w.distance(players.get(2).reaper);
            double enemyDistanceDoof = w.distance(players.get(1).doof) + w.distance(players.get(2).doof);
            int water = w.extra;
            double score = myDistance * 4 - enemyDistance - enemyDistanceDoof * 0.7 - w.score * 850;
            w.score = score;
            
            for( Skill o : oilPools) {
                if(System.currentTimeMillis()-time>40){
                    break;
                }
                double theDistance = o.distance(w);
                if(theDistance < ( w.radius + o.radius )*0.45 ) {
                    score = 6000;
                }
            }
            
            if(score < scoreWreck){
                
                scoreWreck = score;
                bestReaper = new Skill(w.x, w.y, w.radius, w.extra);
                
            }
            
        }
        
        if(wrecks.size() == 0){
            
            bestReaper =  (bestDestroyer);
            
        }
        
        
        
        //Choosing best objective for Doof
        
        bestDoof = new Skill(players.get(bestPlayerId()).reaper);
        System.err.println(bestPlayerId());
        
    }
    
    public static boolean willEarnWater(Skill s){
        for(Skill w : wrecks) {
            if(w.distance(s) < w.radius+200){
                return true;
            }
        }
        return false;
    }
    
    public static int bestPlayerId() {
        
        int bestScore = 0;
        int bestPlayerId = 1;
        
        for(int i = 1 ; i < players.size() ; i++){
            
            Master m = players.get(i);
            
            if( m.score >= bestScore){
                bestScore = m.score;
                bestPlayerId = i;
            }
        }
        
        return bestPlayerId;
        
    }
    
    public static void brake(){
        
        //Brake Reaper
        
        bestReaper.x = bestReaper.x - players.get(0).reaper.vx;
        bestReaper.y = bestReaper.y - players.get(0).reaper.vy;
        
        //Brake Destroyer
        
        //bestDestroyer.x = bestDestroyer.x - players.get(0).destroyer.vx;
        //bestDestroyer.y = bestDestroyer.y - players.get(0).destroyer.vy;
        
    }
    
    public static void clean() {
        
        tankers.clear();
        
        previousPlayers.clear();
        previousPlayers.addAll(players);
        players.clear();
        
        wrecks.clear();
        tarPools.clear();
        oilPools.clear();
        
    }
    
    
    public static class Point {
        
        public int x;
        public int y;
        
        public Point(int x, int y){
            
            this.x = x;
            this.y = y;
            
        }
        
        
        
        public double distance(int x, int y){
            
            return Math.sqrt(distanceSquare(x,y));
            
        }
        
        public double distance(Point p){
            
            return Math.sqrt(distanceSquare(p));
            
        }
        
        public double distanceSquare(int x, int y){
            
            return (this.x - x) * (this.x - x) + (this.y - y) * (this.y - y);
            
        }
        
        public double distanceSquare(Point p){
            
            return (this.x - p.x) * (this.x - p.x) + (this.y - p.y) * (this.y - p.y);
            
        }
        
    }
    
    public static class Unit extends Point {
        
        public float mass;
        public int radius;
        public int vx;
        public int vy;
        public int extra;
        public int extra2;
        
        public Unit(int x, int y, float mass, int radius, int vx, int vy, int extra, int extra2){
            
            super(x, y);
            
            this.mass = mass;
            this.radius = radius;
            this.vx = vx;
            this.vy = vy;
            this.extra = extra;
            this.extra2 = extra2;
            
        }
        
    }
    
    public static class Skill extends Point {
        
        public int radius;
        public int extra;
        public double score;
        
        public Skill(int x, int y, int radius, int extra){
            
            super(x, y);
            
            this.radius = radius;
            this.extra = extra;
            
        }
        
        public Skill(Unit u){
            
            super(u.x, u.y);
            
            this.radius = u.radius;
            this.extra = u.extra;
            
        }
    }
    
    public static class Master {
        
        public int score;
        public int rage;
        public Unit reaper;
        public Unit destroyer;
        public Unit doof;
        
        public Master(int score, int rage)  {
            
            this.score = score;
            this.rage = rage;
            
        }
        
    }
    
}
